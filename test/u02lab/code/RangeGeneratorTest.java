package u02lab.code;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.Assert.*;

public class RangeGeneratorTest {

    private RangeGenerator generator;
    private static final int START = 1;
    private static final int STOP = 10;
    private static final int ATTEMPTS = 5;

    @Before
    public void setUp() throws Exception {
        this.generator = new RangeGenerator(START,STOP);
    }

    @Test
    public void firstCallReturnsStart() {
        assertTrue((this.testStart()));
    }

    @Test
    public void nthCallReturnsNthNumber(){
        this.iterateGenerator(ATTEMPTS);
        Optional<Integer> number = generator.next();
        assertTrue(number.get().equals(START + ATTEMPTS));
    }

    @Test
    public void lastCallReturnsLastNumber(){
        this.iterateGenerator(STOP-1);
        Optional<Integer> number = generator.next();
        assertTrue(number.get().equals(STOP));
    }

    @Test
    public void callOverRangeIsEmpty(){
        this.iterateGenerator(STOP);
        Optional<Integer> number = generator.next();
        assertTrue(number.equals(Optional.empty()));
    }

    @Test
    public void nextAfterResetIsStart(){
        this.iterateGenerator(ATTEMPTS);
        this.generator.reset();
        assertTrue(this.testStart());

    }

    @Test
    public void isNotOverDuringGeneration(){
        this.iterateGenerator(ATTEMPTS);
        assertFalse(this.generator.isOver());
    }

    @Test
    public void isOverAfterGeneration(){
        this.iterateGenerator(STOP);
        assertTrue(this.generator.isOver());
    }

    @Test
    public void canReturnRemainingNumbers(){
        this.iterateGenerator(ATTEMPTS);
        List<Integer> remaining = this.generator.allRemaining();
        assertTrue(this.compareList(remaining));
    }

    @Test
    public void cannotReturnRemainingAfterStop(){
        this.iterateGenerator(STOP);
        List<Integer> remaining = this.generator.allRemaining();
        assertTrue(remaining.isEmpty());
    }

    private boolean testStart(){
        Optional<Integer> number = generator.next();
        return number.get().equals(START);
    }

    private void iterateGenerator(final int n){
        for(int i = 0; i < n; i++){
            generator.next();
        }
    }

    private boolean compareList(final List<Integer> remaining){
        List<Integer> trueRemaining = new ArrayList<>();
        for(int i = ATTEMPTS + 1 ; i <= STOP; i++){
            trueRemaining.add(i);
        }
        return trueRemaining.equals(remaining);
    }





}