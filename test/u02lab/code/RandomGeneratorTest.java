package u02lab.code;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class RandomGeneratorTest {

    private static final int RANGE = 10;
    private RandomGenerator generator;

    @Before
    public void setUp() throws Exception {
        this.generator = new RandomGenerator(RANGE);
    }

    @Test
    public void allAreBinary(){
        boolean allBinary = true;
        int i = RANGE;
        while (i < RANGE && allBinary){
            allBinary = this.generator.next().equals(0) || this .generator.next().equals(1);
            i++;
        }
        assertTrue(allBinary);
    }

    @Test
    public void isOverAfterGeneration(){
        int i = 0;
        while (i <= RANGE ){
            this.generator.next();
            i++;
        }
        boolean over = this.generator.isOver();
        assertTrue(over);
    }

    @Test
    public void notOverDuringComputation(){
        int i = 0;
        boolean over = false;
        while (i < RANGE-1){
            this.generator.next();
            over = this.generator.isOver();
            i++;
        }
        assertFalse(over);
    }

    @Test
    public void generateSameNumbersAfterReset(){
        List<Integer> generated = this.populateList();
        this.generator.reset();
        assertTrue(generated.equals(this.populateList()));
    }

    @Test
    public void emptyAfterComputation(){
        this.populateList();
        assertTrue(this.generator.next().equals(Optional.empty()));
    }

    @Test
    public void canGetRemaining(){
        this.generator.next();
        List<Integer> remaining = this.generator.allRemaining();
        assertTrue(remaining.size() == RANGE - 1);
    }

    @Test
    public void emptyAfterGetAll(){
        this.generator.allRemaining();
        assertTrue(this.generator.next().equals(Optional.empty()));
    }

    private List<Integer> populateList(){
        List<Integer> generated = new ArrayList<>();
        for(int i = 0; i < RANGE; i++){
            generated.add(this.generator.next().get());
        }
        return generated;
    }

}