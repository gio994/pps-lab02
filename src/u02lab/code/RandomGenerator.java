package u02lab.code;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

/**
 * Step 2
 *
 * Now consider a RandomGenerator. Using TDD approach (create small test, create code that pass test, refactor
 * to excellence) implement the below class that represents a sequence of n random bits (0 or 1). Recall
 * that math.random() gives a double in [0,1]..
 * Be sure to test all that is needed, as before
 *
 * When you are done:
 * A) try to refactor the code according to DRY (RandomGenerator vs RangeGenerator), using patterns as needed
 * - be sure tests still pass
 * - refactor the test code as well
 * B) create an abstract factory for these two classes, and implement it
 */
public class RandomGenerator implements SequenceGenerator {

    private int length;
    private int generated;
    private List<Integer> generatedNumbers;

    public RandomGenerator(final int n){
        this.length = n;
        this.generated = 0;
        this.generatedNumbers =this.generateRandom();

    }

    @Override
    public Optional<Integer> next() {
        return this.isOver() ? Optional.empty(): Optional.of(this.generatedNumbers.get(generated++));
    }

    @Override
    public void reset() {
        this.generated=0;
    }

    @Override
    public boolean isOver() {
        return this.length <= this.generated;
    }

    @Override
    public List<Integer> allRemaining(){
        int n = this.generated;
        this.generated = this.length;
        return this.generatedNumbers.subList(n,length);
    }

    private List<Integer> generateRandom(){
        List<Integer> numbers = new ArrayList<>();
        double number ;
        for (int i = 0; i < this.length; i++){
            number = Math.random();
            numbers.add(number > 0.50 ? 1 : 0);
        }
        return numbers;
    }
}
