package u02lab.code;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Step 1
 *
 * Using TDD approach (create small test, create code that pass test, refactor to excellence)
 * implement the below class that represents the sequence of numbers from start to stop included.
 * Be sure to test that:
 * - the produced elements (called using next(), go from start to stop included)
 * - calling next after stop leads to a Optional.empty
 * - calling reset after producing some elements brings the object back at the beginning
 * - isOver can actually be called in the middle and should give false, at the end it gives true
 * - can produce the list of remaining elements in one shot
 */
public class RangeGenerator implements SequenceGenerator {

    private int start;
    private int stop;
    private int current;

    public RangeGenerator(int start, int stop){
        this.start = start;
        this.current = start;
        this.stop = stop;
    }

    @Override
    public Optional<Integer> next(){
        return !this.isOver() ? Optional.of(this.current++) : Optional.empty();
    }

    @Override
    public void reset() {
        this.current = this.start;
    }

    @Override
    public boolean isOver() {
        return this.current > this.stop;
    }

    @Override
    public List<Integer> allRemaining() {
        List<Integer> remaining = new ArrayList<>();
        while(!this.isOver()){
            remaining.add(current++);
        }
        return remaining;
    }
}
